from functools import wraps

from flask import current_app
from flask_httpauth import HTTPBasicAuth
from werkzeug.security import check_password_hash

from .exceptions import ProcessingError

auth = HTTPBasicAuth()


def admin_required(fn):
    @wraps(fn)
    def wrapper(*args, **kwargs):
        if not current_app.auth_backend.is_admin(auth.username()):
            raise ProcessingError("Forbidden to run without admin permission", 403)
        return fn(*args, **kwargs)

    return wrapper


auth.admin_required = admin_required


def current_user_is_admin():
    current_user = auth.username()
    return current_app.auth_backend.is_admin(current_user)


def permission_to_read(api_root, collection_id):
    collection_info = current_app.medallion_backend.get_collection(api_root, collection_id)
    if not collection_info["can_read"]:
        return False
    if current_user_is_admin():
        return True
    return current_app.auth_backend.has_permission(auth.username(), collection_id)


def permission_to_write(api_root, collection_id):
    collection_info = current_app.medallion_backend.get_collection(api_root, collection_id)
    if not collection_info["can_write"]:
        return False
    return current_user_is_admin()


@auth.verify_password
def verify_basic_auth(username, password):
    password_hash = current_app.auth_backend.get_password_hash(username)
    return check_password_hash(password_hash, password)
