import datetime

from werkzeug.security import generate_password_hash

from .base import AuthBackend
from ...exceptions import ProcessingError


class AuthMemoryBackend(AuthBackend):

    def __init__(self, users):
        self.users = {}
        for user in users:
            self.add_user(**user)

    def get_password_hash(self, username):
        if username not in self.users:
            raise ProcessingError("User '{}' not exist".format(username), 404)
        return self.users[username]["password_hash"]

    def get_user_info(self, username):
        if username not in self.users:
            raise ProcessingError("User '{}' not exist".format(username), 404)
        user = self.users[username]
        return {
            "is_admin": user["is_admin"],
            "permission": user["permission"],
            "company_name": user["company_name"],
            "contact_name": user["contact_name"]
        }

    def is_admin(self, username):
        if username not in self.users:
            raise ProcessingError("User '{}' not exist".format(username), 404)
        return self.users[username]['is_admin']

    def has_permission(self, username, collection_id):
        if username not in self.users:
            raise ProcessingError("User '{}' not exist".format(username), 404)
        user_permission = self.users[username]['permission']
        if collection_id not in user_permission:
            return False
        if user_permission[collection_id] is None:
            return True
        return user_permission[collection_id] > datetime.datetime.now()

    def add_user(self, username, password, is_admin=False, permission=None, company_name=None, contact_name=None):
        if permission is None:
            permission = {}
        if username in self.users:
            raise ProcessingError("User '{}' already exist".format(username), 409)

        self.users[username] = {
            "username": username,
            "password_hash": generate_password_hash(password),
            "is_admin": is_admin,
            "permission": permission,
            "company_name": company_name,
            "contact_name": contact_name
        }
