class AuthBackend(object):

    def get_password_hash(self, username):
        """Given a username provide the password hash for verification."""
        raise NotImplementedError()

    def get_user_info(self, username):
        """Given an username provide the user info."""
        raise NotImplementedError()

    def is_admin(self, username):
        """Given an username provide the user is admin or not."""
        raise NotImplementedError()

    def has_permission(self, username, collection_id):
        """Given an username provide the user has permission to read the collection"""
        raise NotImplementedError()

    def add_user(self, username, password, is_admin=False, permission=None, company_name=None, contact_name=None):
        """Create a new user."""
        raise NotImplementedError()
