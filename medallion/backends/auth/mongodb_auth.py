import datetime
import logging

from werkzeug.security import generate_password_hash

from .base import AuthBackend
from ...exceptions import ProcessingError

try:
    from pymongo import MongoClient
    from pymongo.errors import ConnectionFailure
except ImportError:
    raise ImportError("'pymongo' package is required to use this module.")

# Module-level logger
log = logging.getLogger(__name__)


class AuthMongodbBackend(AuthBackend):
    def __init__(self, uri, **kwargs):
        try:
            self.client = MongoClient(uri)
            self.db_name = kwargs["db_name"]
            for user in kwargs['users']:
                try:
                    self.add_user(**user)
                except ProcessingError:
                    continue
        except ConnectionFailure:
            log.error("Unable to establish a connection to MongoDB server {}".format(uri))

    def get_password_hash(self, username):
        user_obj = self.client[self.db_name]['users'].find_one(
            filter={"username": username}
        )
        if not user_obj:
            raise ProcessingError("User '{}' not exist".format(username), 404)
        return user_obj['password_hash']

    def get_user_info(self, username):
        user_obj = self.client[self.db_name]['users'].find_one(
            filter={"username": username},
            projection={"_id": 0, "password_hash": 0}
        )
        if not user_obj:
            raise ProcessingError("User '{}' not exist".format(username), 404)
        return user_obj

    def is_admin(self, username):
        user_obj = self.client[self.db_name]['users'].find_one(
            filter={"username": username}
        )
        if not user_obj:
            raise ProcessingError("User '{}' not exist".format(username), 404)
        return user_obj["is_admin"]

    def has_permission(self, username, collection_id):
        user_obj = self.client[self.db_name]['users'].find_one(
            filter={"username": username}
        )
        if not user_obj:
            raise ProcessingError("User '{}' not exist".format(username), 404)
        if collection_id not in user_obj["permission"]:
            return False
        if user_obj["permission"][collection_id] is None:
            return True
        return user_obj["permission"][collection_id] > datetime.datetime.now()

    def add_user(self, username, password, is_admin=False, permission=None, company_name=None, contact_name=None):
        user_obj = self.client[self.db_name]['users'].find_one(
            filter={"username": username}
        )
        if user_obj:
            raise ProcessingError("User '{}' already exist".format(username), 409)
        self.client[self.db_name]['users'].insert_one({
            "username": username,
            "password_hash": generate_password_hash(password),
            "is_admin": is_admin,
            "permission": permission,
            "company_name": company_name,
            "contact_name": contact_name,
            "created": datetime.datetime.now(),
            "updated": datetime.datetime.now()
        })
